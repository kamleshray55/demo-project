variable "ami" {
    description = "Amazon Machine Image value"
    default = "ami-0d2986f2e8c0f7d01"
}

variable "instance_type"{
    description = "Amazon Instance instance type, like t2.micro,t2.medium"
    default = "t2.micro" 
}

variable "instances" {
    description = "No of ec2 instance" 
    default = "2"
}