resource "aws_instance" "AWSEC2Instance" {
  ami             = "ami-0d2986f2e8c0f7d01"
  instance_type   = "t2.micro"
  security_groups = ["launch-wizard-1"]
  key_name        = "Custom-VPC"
  tags = {
    Name = "EC2-Instance by terraform"
  }
}